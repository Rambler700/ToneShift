# -*- coding: utf-8 -*-
"""
Created on Wed Feb  7 15:29:23 2018

@author: enovi
"""

#toneshift
#changes tone between positive, negative, and neutral
#every word in a list is a synonym for the word in the same spot in the other lists
#for example, positive_words[20], neutral_words[20], and negative_words[20] have the same meaning
#but each word has a different emotional impact
def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Tone Shift!')
    while choice == True:
         c = input('Change the tone of an article y/n? ')
         if c == 'y':
             article_text = get_text()
             tone_shift(article_text)
             choice = True
         elif c == 'n':
             choice = False

def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def tone_shift(article_text):
    positive_words = ['freethinker','steadfast','benevolent','charitable','fastidious','decisive',
                      'attentive','whistleblower','undemanding','cautious','brave','conversational',
                      'beautiful','scholarly','poet','inspired','nurturing','firm',
                      'engaged','enthusiastic','honest','intelligent','passionate','unconventional',
                      'imbiber','indignant','opportunity','delayed','observant','perfect',
                      'claims','unpopular','requesting','approve','organize','justification',
                      'lessen','pushed','nudged','nudge','noteworthiness','mindful',
                      'asserted','reverence','discussing','secret','accessed','opponents',
                      'effort','dismantle','requit','funds','assets','protection',
                      'meaningful','monitor','manage','administer','disagreeable','agreeable',
                      'praised','praise','tone','advancing','informant','increasing',
                      'assist','chronicled','verified','contradict','endorsed','endorse',
                      'disguise','avoiding','aiding','announced','announce','lecturing',
                      'lessened','lessen','disbursed','disburse','depleted','deplete',
                      'criticize','direct','inactive','idle','spurn','concede','misguided',
                      'disapproving','details','testimony','revelation','disconcerted',
                      'assert','rigorous','thriving','endure','commotion','emerge',
                      'disagreeable','disunite','reprisal','factional','gush','contradict',
                      'entreat','grift','peace','elusive','stymie','approaching','browbeat',
                      'rival','difficulty','adamant']
    
    neutral_words  = ['eccentric','stubborn','naive','giving','demanding','obstinate',
                      'finicky','leaker','lax','timid','aggressive','talkative',
                      'attractive','educated','writer','copied','indulgent','harsh',
                      'attentive','lively','precise','clever','emotional','different',
                      'drinker','annoyed','setback','tardy','bureaucratic','acceptable',
                      'allegations','disliked','urging','permit','arrange','proof',
                      'minimize','forced','pressured','pressure','importance','aware',
                      'stated','respect','talking','private','compromised','adversaries',
                      'exertion','disassemble','avenge','money','properties','security',
                      'significant','observe','operate','govern','uncomfortable','comfortable',
                      'complimented','compliment','perspective','continuing','source','rising',
                      'support','described','confirmed','disagree','authorized','authorize',
                      'conceal','dodging','assisting','revealed','reveal','criticizing',
                      'declined','decline','spent','spend','exhausted','exhaust',
                      'insult','command','lazy','indolent','reject','admit','incorrect',
                      'disliking','information','correspondence','announcement','troubled',
                      'declare','demanding','healthy','distress','protest','appear',
                      'acrimonious','isolate','repercussion','partisan','vent','discredit',
                      'request','hoax','concord','evasive','inhibit','impending','intimidate',
                      'opponent','trouble','resolute']
    
    negative_words = ['kook','closeminded','gullible','sucker','strict','unimaginative',
                      'pedantic','snitch','negligent','cowardly','reckless','loudmouthed',
                      'gaudy','overeducated','hack','plagiarized','pushover','abusive',
                      'obsessed','uncontrolled','rude','promising','hysterical','perverse',
                      'drunk','fussy','disaster','late','picky','okay',
                      'accusations','hated','lobbying','condone','orchestrate','evidence',
                      'slash','shoved','coerced','coerce','significance','alert',
                      'disclosed','courtesy','chattering','undisclosed','hacked','enemies',
                      'toil','demolish','punish','cash','possessions','defense',
                      'suggestive','surveil','control','supervise','intolerable','tolerable',
                      'flattered','flatter','bias','ongoing','spy','escalating',
                      'sustain','reported','corroberated','repudiate','validated','validate',
                      'obscure','evading','abetting','exposed','expose','haranguing',
                      'deteriorated','deteriorate','squandered','squander','consumed','consume',
                      'trash','order','slothful','lethargic','refuse','confess','false',
                      'hating','dirt','hearsay','disclosure','shaken',
                      'avow','stringent','hardy','suffer','uproar','erupt',
                      'bitter','alienate','backlash','biased','belch','debunk',
                      'invoke','fraud','order','shifty','obstruct','imminent','threaten',
                      'enemy','ordeal','insistent']
    
    i              = 0
    c              = ''    
    #select spin    
    c = input('Make the message (p)ositive, n(e)utral, or (n)egative? ')
    if c == 'p':
    #shift to positive  
        print('Positive tone selected')
        for i in range(0, len(positive_words)):
            if article_text.count(neutral_words[i]) > 0:
                article_text = article_text.replace(neutral_words[i],positive_words[i])
            if article_text.count(negative_words[i]) > 0:
                article_text = article_text.replace(negative_words[i],positive_words[i])    
    #shift to neutral    
    if c == 'e':
        print('Neutral tone selected')
        for i in range(0, len(neutral_words)):
            if article_text.count(positive_words[i]) > 0:
                article_text = article_text.replace(positive_words[i],neutral_words[i])
            if article_text.count(negative_words[i]) > 0:
                article_text = article_text.replace(negative_words[i],neutral_words[i])    
    #shift to negative
    if c == 'n':
        print('Negative tone selected')
        for i in range(0, len(negative_words)):
            if article_text.count(positive_words[i]) > 0:
                article_text = article_text.replace(positive_words[i],negative_words[i])
            if article_text.count(neutral_words[i]) > 0:
                article_text = article_text.replace(neutral_words[i],negative_words[i])    
    #print message    
    print(article_text)

main()